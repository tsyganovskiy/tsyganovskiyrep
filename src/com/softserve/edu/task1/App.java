package com.softserve.edu.task1;

import java.io.*;

public class App {
	/**
	 * .
	 * 
	 * @param args
	 *     
	 * @throws IOException
	 *             description
	 */

	public static void main(String[] args) {

		if (args.length == 0) {
			System.out.println("Input error.The programe will be closed");
			System.exit(0);
		}
		int width = 0;
		String inputWidth = args[0];

		char[] checkwidth = inputWidth.toCharArray();
		for (int i = 0; i < checkwidth.length; i++) {
			if (checkwidth[i] >= '0' && checkwidth[i] <= '9') {
				width = Integer.parseInt(inputWidth);
			} else {
				System.out.println("Input error.The programe will be closed");
				System.exit(0);
			}
		}

		int heigth = 0;
		String inputHeigth = args[1];

		char[] checkheigth = inputHeigth.toCharArray();
		for (int i = 0; i < checkheigth.length; i++) {
			if (checkheigth[i] >= '0' && checkheigth[i] <= '9') {
				heigth = Integer.parseInt(inputHeigth);
			} else {
				System.out.println("Input error.The programe will be closed");
				System.exit(0);
			}
		}

		ChessBoard chessboard = new ChessBoard(width, heigth);
		chessboard.show();

	}
}
