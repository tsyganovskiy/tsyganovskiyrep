package com.softserve.edu.task1;

public class ChessBoard {

  private int width;
  private int height;

  ChessBoard(int width, int height) {
    this.width = width;
    this.height = height;
  }
  /**.
   *  Show chess board in console...
   */
  public void show() {

    char[][] starsArray = new char[height][width];

    for (int i = 0; i < height; i++) {   
      for (int j = 0; j < width; j++) {
        starsArray[i][j] = '*';

      }

    }

    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        if (!(i % 2 == 0) && (j % 2 == 0)) {
          starsArray[i][j] = ' ';
        } else if ((i % 2 == 0) && (!(j % 2 == 0))) {
          starsArray[i][j] = ' ';
        }
      }
    }

    for (int i = 0; i < height; i++) {
      System.out.println();
      for (int j = 0; j < width; j++) {
        System.out.print(starsArray[i][j]);

      }
    }
  }
}
