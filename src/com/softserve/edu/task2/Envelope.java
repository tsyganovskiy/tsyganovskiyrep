package com.softserve.edu.task2;

public class Envelope {
  private double width;
  private double height;

  Envelope(double width, double height) {
    this.width = width;
    this.height = height;
  }
  /**.
   * Compare two envelopes
   * 
   * @param obj another envelope
   */
  public void putInside(Envelope obj) {
    if (((this.width >= obj.width) || (this.height >= obj.height))
        && ((this.width >= obj.height) || (this.height >= obj.width))) {
      System.out.println("Envelope �1 cann't be put in �2");
    } else {
      System.out.println("Envelope �1 can be put in �2");
    }

  }
}
