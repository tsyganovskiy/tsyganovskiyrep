package com.softserve.edu.task3;

import java.io.IOException;
import java.util.*;

public class App {

	public static void main(String[] args) {
		int exitsign = 0;
		List<Triangle> tr = new ArrayList<Triangle>();
		do {
			String str = "";
			System.out.println("Input triangle as : name,side1,side2,side3");
			Scanner sc = new Scanner(System.in);
			str = sc.nextLine().toLowerCase();
			int getcommaposition = 0;
			List<String> strparcer = new ArrayList<String>();
			for (int i = 0; i < str.length(); i++) {
				if ((str.charAt(i) == ',')) {
					strparcer.add(str.substring(getcommaposition, i));
					getcommaposition = i + 1;
				}
			}
			strparcer.add(str.substring(getcommaposition, str.length()));
			// ��������� �� ������������ �������� ������

			String name = strparcer.get(0).trim();
			double side1 = 0;
			double side2 = 0;
			double side3 = 0;

			try {
				side1 = Double.parseDouble(strparcer.get(1).trim());
				side2 = Double.parseDouble(strparcer.get(2).trim());
				side3 = Double.parseDouble(strparcer.get(3).trim());
			} catch (Exception e) {
				System.out.println("You inputed wrong value");
			}
			if (((side1 + side2) > side3) && ((side1 + side3) > side2) && (side2 + side3) > side1) {
				tr.add(new Triangle(name, side1, side2, side3));
			} else {
				System.out.println("Inputed sides are not triangle's sides");
			}

			// ���������� ����� �� ������������ ������ ��� �����������
			System.out.println("Do you want add another one? ( press \"y\" / \"yes\")");
			Scanner sc1 = new Scanner(System.in);
			String yesnochoice;
			yesnochoice = sc1.nextLine().toLowerCase();
			if (yesnochoice.equals("yes") || yesnochoice.equals("y")) {
				exitsign = 2;
			} else {

				double[] arrayofsquares = new double[tr.size()];
				for (int i = 0; i < tr.size(); i++) {
					arrayofsquares[i] = tr.get(i).square();
				}
				List<Triangle> trsorted = new ArrayList<Triangle>();
				Arrays.sort(arrayofsquares);

				for (int i = 0; i < arrayofsquares.length; i++) {
					for (int j = 0; j < tr.size(); j++) {
						if (arrayofsquares[i] == tr.get(j).square()) {
							trsorted.add(tr.get(j));
						}
					}
				}

				System.out.println("Result is :");
				for (int j = trsorted.size() - 1; j >= 0; j--) {
					System.out.println(trsorted.get(j).name + " " + trsorted.get(j).square());
				}
				sc.close();
				sc1.close();
				System.exit(0);
			}

		} while (exitsign != 1);
	}
}
