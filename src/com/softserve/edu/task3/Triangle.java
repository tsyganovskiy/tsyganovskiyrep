package com.softserve.edu.task3;

public class Triangle  {
  String name;
  double side1;
  double side2;
  double side3;
  
  Triangle (String name, double side1, double side2, double side3 ){
	this.name = name;
	this.side1 = side1;
	this.side2 = side2;
	this.side3 = side3;
  }

  public double square() {
	double sqr = 0;
    sqr = Math.sqrt(0.5 * (side1 + side2 + side3)
                   * ((0.5 * (side1 + side2 + side3) - side1))
                   * ((0.5 * (side1 + side2 + side3)) - side2)
                   * ((0.5 * (side1 + side2 + side3)) - side3));
    return sqr;  
  }
 
}

