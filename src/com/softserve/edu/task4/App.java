package com.softserve.edu.task4;
import java.io.*;
import java.util.*;


public class App {
  /**..
   * ��� ������ ����� ������� ��������� ���� � ����� ����� C:
   * � ������ my.txt 
   * � ��������� 2 ��������
   * 1. ����/������ ������
   * 2. ����/������ ������/������ ������
   *  �������� ���������� ������������� 
   *  � ����������� �� ���-�� ����������  
   * @param args input strings
   */
  public static void main(String[] args)throws Exception {
    if (args == null || args.length < 2 || args.length > 3 ) {
      System.out.println("Must be 2 or 3 input values.");
    }
    
    if (args.length == 2) {
      try {
        BufferedReader in = new BufferedReader(new FileReader(args[0]));
        Scanner sc = new Scanner(in);
        int result = 0;
        String str;
        String substr;
        
        substr = args[1].toLowerCase();
       
        while (sc.hasNextLine()) {  
          str = sc.nextLine().toLowerCase();
          for (int i = 0; i <= (str.length() - substr.length()); i++) {
            if (str.substring(i, i + substr.length()).equalsIgnoreCase(substr)) {
              result++;
            }
          }
        }     
        sc.close();
        in.close();
      
        System.out.println("The string " + "\"" + substr + "\"" + " is met  " + result + " times");
      
          
      }  catch (IOException e) {
        System.out.println("Cann't find the file" );
      } 
    }
    
    if (args.length == 3) {
      try {
        BufferedReader in = new BufferedReader(new FileReader(args[0]));
        
        Scanner sc = new Scanner(in);
        String substr;
        String strtoreplace;
        
        substr = args[1].toLowerCase();
        strtoreplace = args[2].toLowerCase();
                
        while (sc.hasNextLine()) {
          StringBuffer str = new StringBuffer(sc.nextLine().toLowerCase());
          for (int i = 0; i <= (str.length() - substr.length()); i++) {
            if (str.substring(i, i + substr.length()).equalsIgnoreCase(substr)) {
              str.insert(i, strtoreplace);
              str.delete(i + strtoreplace.length(),i + strtoreplace.length() + substr.length());
            }
          }
          str.append('\n');
          try {
            File file = new File("c:\\my2.txt");
            if (!file.exists()) {
              file.createNewFile();
            }
            FileWriter fw = new FileWriter(file,true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(str.toString());
            bw.close();
            fw.close();
          } catch (IOException e) {
            System.out.println("Cann't find the file");
          }
          //System.out.println(str);
        }
        System.out.println("New re-written file is created");
        sc.close();
        in.close();
      }  catch (IOException e) {
        System.out.println("Cann't find the file");
      }
    }
  }
}

