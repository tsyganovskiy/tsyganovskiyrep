package com.softserve.edu.task6;
import java.io.*;
import java.util.*;

public class App {

  public static void main(String[] args) {
    
	String str = args[0];
	String str_verified = "";
	
    if (str.length() != 6) {
        System.out.println("Input error.The programe will be closed");
        System.exit(0);
      }
    
	char[] checkheigth = str.toCharArray();
	for (int i = 0; i < checkheigth.length; i++) {
		if (checkheigth[i] >= '0' && checkheigth[i] <= '9') {
			str_verified = str;
		} else {
			System.out.println("Input error.The programe will be closed");
			System.exit(0);
		}
	}
	
    if (str.length() != 6) {
      System.out.println("Input error.The programe will be closed");
      System.exit(0);
    }
        
   
    try {
      Ticket tick = new Ticket();
      Scanner scanner = new Scanner(new File(args[1]));
      String sign = scanner.nextLine();
     
      if (sign.equals("Moscow")) {
        tick.getNumberOfHappyTicketsMoscow(str_verified);
      } else if (sign.equals("Piter")) {
        tick.getNumberOfHappyTicketsPiter(str_verified);
      } 
      scanner.close();

      
    } catch (IOException  e) {
      System.out.println("File cannot be found. Check the path and restart");
    }
  }
}
