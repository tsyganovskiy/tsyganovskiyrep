package com.softserve.edu.task6;
import java.util.*;

public class Ticket {
  /**..
   * 
   * @return  number of happy tickets....
   */
  
  public void getNumberOfHappyTicketsMoscow(String ticket) {
	  
    List<Integer> first_three_number = new ArrayList<Integer>();
    List<Integer> second_three_number = new ArrayList<Integer>();  
    
    int[] ticketnumders = new int[6];
    try {
      ticketnumders [0] = Integer.parseInt(ticket. substring(0,1));
      ticketnumders [1] = Integer.parseInt(ticket.substring(1,2));
      ticketnumders [2] = Integer.parseInt(ticket.substring(2,3));
      ticketnumders [3] = Integer.parseInt(ticket.substring(3,4));
      ticketnumders [4] = Integer.parseInt(ticket.substring(4,5));
      ticketnumders [5] = Integer.parseInt(ticket.substring(5));
    } catch (Exception e) {
      System.out.println("Input error.The programe will be closed");
      System.exit(0);
    }
    
    for (int i1 = 0; i1 <= ticketnumders [0]; i1++) {
      for (int j1 = 0; j1 <= ticketnumders [1]; j1++) {
        for (int k1 = 0; k1 <= ticketnumders [2]; k1++) {
          first_three_number.add(i1 + j1 + k1);
        }
      }
    }
    
    for (int i2 = 0; i2 <= ticketnumders [3]; i2++) {
      for (int j2 = 0; j2 <= ticketnumders [4]; j2++) {
        for (int k2 = 0; k2 <= ticketnumders [5]; k2++) {
          second_three_number.add(i2 + j2 + k2);
        }
      }
    }
    
    int res = 0;
    for (int count1 = 0; count1 < first_three_number.size(); count1++) {
      for (int count2 = 0; count2 < second_three_number.size(); count2++) {
        if (first_three_number.get(count1) == second_three_number.get(count2)) {
          res++;  
        }
      }          
    }
    System.out.println("Number of happy tickets (Moscow) is " + res);
  }
  
  public void getNumberOfHappyTicketsPiter(String ticket){

    int[] ticketnumders = new int[6];
    List<Integer> listOdd = new ArrayList<Integer>();
    List<Integer> listEven = new ArrayList<Integer>();
    try {
      ticketnumders [0] = Integer.parseInt(ticket. substring(0,1));
      ticketnumders [1] = Integer.parseInt(ticket.substring(1,2));
      ticketnumders [2] = Integer.parseInt(ticket.substring(2,3));
      ticketnumders [3] = Integer.parseInt(ticket.substring(3,4));
      ticketnumders [4] = Integer.parseInt(ticket.substring(4,5));
      ticketnumders [5] = Integer.parseInt(ticket.substring(5));
    } catch (Exception e) {
      System.out.println("Input error.The programe will be closed");
      System.exit(0);
    }
    
    for (int i1 = 0; i1 <= ticketnumders [0]; i1++) {
        for (int j1 = 0; j1 <= ticketnumders [2]; j1++) {
          for (int k1 = 0; k1 <= ticketnumders [4]; k1++) {
        	  listOdd.add(i1 + j1 + k1);  
          }
        }
      }
    
    for (int i2 = 0; i2 <= ticketnumders [1]; i2++) {
        for (int j2 = 0; j2 <= ticketnumders [3]; j2++) {
          for (int k2 = 0; k2 <= ticketnumders [5]; k2++) {
        	  listEven.add(i2 + j2 + k2);
        
          }
        }
      }  
 
    int res = 0;
    for (int count1 = 0; count1 < listOdd.size(); count1++) {
      for (int count2 = 0; count2 < listEven.size(); count2++) {
        if (listOdd.get(count1) == listEven.get(count2)) {
          res++;  
        }
      }          
    }  
      System.out.println("Number of happy tickets (St.Petersburg) is " + res);
  }
}
