package com.softserve.edu.task7;

import java.util.Scanner;

public class App {

  public static void main(String[] args) {
    System.out.println("Input integer positive number");
    Scanner sc1 = new Scanner(System.in);
    String  str = sc1.nextLine();
    int number = 0;
    try {
      number = Integer.parseInt(str);
    } catch (Exception e) {
      System.out.println("Input error.The programe will be closed");
      System.exit(0);
    }
    if (number < 0) {
      System.out.println("Input error.The programe will be closed");
      System.exit(0);
    }
    String res = "";    
    for (int i = 0; i < number; i++ ) {
      if ((i * i) < number) {
        res +=  i + ",";  
      }
    }
    sc1.close();
    System.out.print("Result is : " + res.substring(0, res.length() - 1));
  }

}
