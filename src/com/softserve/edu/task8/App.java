package com.softserve.edu.task8;
import java.util.*;

public class App {

  public static void main(String[] args) {
  
    int begin = 0;
    int end = 0;

    try {
      begin = Integer.parseInt(args[0]);
      end = Integer.parseInt(args[1]);
    } catch (Exception e) {
      System.out.println("Input error.The programe will be closed");
      System.exit(0);
    }
    if (end < 0 || begin < 0) {
      System.out.println("Input error.The programe will be closed");
      System.exit(0);
    }
    
    Fibonachi fb = new Fibonachi(begin,end);
    fb.show();
  }
}

